package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh12N028 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the natural number");
        int[][] result = spiralArray(scanner.nextInt());
        printArr(result);
    }

    public static int[][] spiralArray(int n) {
        int[][] matrix = new int[n][n];
        int totalNums = n * n;
        int x = 0;
        int y = -1;
        int indexRight = n - 1;
        int indexLeft = 0;
        int indexUp = 1;
        int indexDown = n - 1;
        int currentNumber = 0;

        while (currentNumber < totalNums) {
            while (currentNumber < totalNums && y < indexRight) {
                y++;
                currentNumber++;
                matrix[x][y] = currentNumber;
            }
            indexRight--;

            while (currentNumber < totalNums && x < indexDown) {
                x++;
                currentNumber++;
                matrix[x][y] = currentNumber;
            }
            indexDown--;

            while (currentNumber < totalNums && y > indexLeft) {
                y--;
                currentNumber++;
                matrix[x][y] = currentNumber;
            }
            indexLeft++;

            while (currentNumber < totalNums && x > indexUp) {
                x--;
                currentNumber++;
                matrix[x][y] = currentNumber;
            }
            indexUp++;
        }
        return matrix;
    }

    public static void printArr(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }
}
