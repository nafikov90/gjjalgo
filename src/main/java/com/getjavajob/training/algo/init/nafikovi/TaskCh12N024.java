package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh12N024 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number");
        int number = scanner.nextInt();
        int[][] resultA = fillTheArrayA(number);
        printArray(resultA);
        int[][] resultB = fillTheArrayB(number);
        printArray(resultB);
    }

    public static int[][] fillTheArrayA(int n) {
        int[][] arr = new int[n][n];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (i > 0 && j > 0) {
                    arr[i][j] = arr[i][j - 1] + arr[i - 1][j];
                } else {
                    arr[i][j] = 1;
                }
            }
        }
        return arr;
    }

    public static int[][] fillTheArrayB(int n) {
        int[][] arr = new int[n][n];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                arr[i][j] = (i + j) % 6 + 1;
            }
        }
        return arr;
    }

    public static void printArray(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
}
