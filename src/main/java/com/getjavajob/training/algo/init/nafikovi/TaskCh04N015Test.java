package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh04N015.calculateAge;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N015Test {
    public static void main(String[] args) {
        testAge1();
        testAge2();
        testAge3();
    }

    public static void testAge1() {
        assertEquals("TaskCh04N015Test.testAge1", 29, calculateAge(6, 1985, 12, 2014));
    }

    public static void testAge2() {
        assertEquals("TaskCh04N015Test.testAge2", 28, calculateAge(6, 1985, 5, 2014));
    }

    public static void testAge3() {
        assertEquals("TaskCh04N015Test.testAge3", 29, calculateAge(6, 1985, 6, 2014));
    }
}
