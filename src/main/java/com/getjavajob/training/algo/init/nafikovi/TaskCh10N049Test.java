package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh10N049.ZERO_INDEX;
import static com.getjavajob.training.algo.init.nafikovi.TaskCh10N049.getIndexMaxEl;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N049Test {
    public static void main(String[] args) {
        testMaxIndex1();
        testMaxIndex2();
        testMaxIndex3();
        testMaxIndex4();
    }

    public static void testMaxIndex1() {
        int[] array = {0, 0, 0, 1};
        assertEquals("TaskCh10N049Test.testMaxIndex1", 3, getIndexMaxEl(array, ZERO_INDEX));
    }

    public static void testMaxIndex2() {
        int[] array = {1, 0, 0, 0};
        assertEquals("TaskCh10N049Test.testMaxIndex2", 0, getIndexMaxEl(array, ZERO_INDEX));
    }

    public static void testMaxIndex3() {
        int[] array = {1, 0, 0, 1};
        assertEquals("TaskCh10N049Test.testMaxIndex3", 0, getIndexMaxEl(array, ZERO_INDEX));
    }

    public static void testMaxIndex4() {
        int[] array = {1, 10, 2, 7, 5};
        assertEquals("TaskCh10N049Test.testMaxIndex4", 1, getIndexMaxEl(array, ZERO_INDEX));
    }
}
