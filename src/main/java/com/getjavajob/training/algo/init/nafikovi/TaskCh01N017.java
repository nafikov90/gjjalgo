package com.getjavajob.training.algo.init.nafikovi;

import static java.lang.Math.*;

public class TaskCh01N017 {
    public static void main(String[] args) {
        System.out.println(methodO(1));
        System.out.println(methodP(1, 2, 3, 4));
        System.out.println(methodR(1));
        System.out.println(methodS(1));
    }

    public static double methodO(int x) {
        return sqrt(1 - pow(sin(x), 2));
    }

    public static double methodP(int x, int a, int b, int c) {
        return 1 / sqrt(a * pow(x, 2) + b * x + c);
    }

    public static double methodR(int x) {
        return (sqrt(x + 1) + sqrt(x - 1)) / (2 * sqrt(x));
    }

    public static double methodS(int x) {
        return abs(x) + abs(x + 1);
    }


}
