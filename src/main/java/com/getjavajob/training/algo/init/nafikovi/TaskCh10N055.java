package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh10N055 {
    private static String s = "";

    public static void resetValueOfString() {
        s = "";
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number system (2 <= N <= 16)");
        int nSystem = scanner.nextInt();
        System.out.println("Enter the natural number");
        int number = scanner.nextInt();
        int result = method1(nSystem, number);
        System.out.println(result);
    }

    public static String method(int nSystem, int number) {
        if (number < nSystem) {
            s += "" + number;
            return s;
        } else {
            if (number != 0) {
                method(nSystem, number / nSystem);
                s += "" + number % nSystem;
            }
        }
        return s;
    }

    public static int method1(int nSystem, int number) {
        if (number < nSystem) {
            s += "" + number;
            return Integer.parseInt(s);
        } else {
            if (number != 0) {
                method(nSystem, number / nSystem);
                s += "" + number % nSystem;
            }
        }
        return Integer.parseInt(s);
    }
}
