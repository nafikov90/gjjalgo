package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh04N033 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a natural number");
        int number = scanner.nextInt();
        boolean taskA = lastDigitIsEven(number);
        System.out.println("Last digit is even? " + taskA);
        boolean taskB = lastDigitIsOdd(number);
        System.out.println("Last digit is odd? " + taskB);
    }

    public static boolean lastDigitIsEven(int number) {
        return number % 2 == 0;
    }

    public static boolean lastDigitIsOdd(int number) {
        return Math.abs(number) % 2 == 1;
    }
}
