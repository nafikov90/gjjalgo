package com.getjavajob.training.algo.init.nafikovi;

import java.util.Arrays;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh12N063.averageClass;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N063Test {
    public static void main(String[] args) {
        testCountOfLearners();
    }

    public static void testCountOfLearners() {
        int[][] testArray = new int[][]{
                {5, 5, 5, 5},
                {10, 10, 10, 10},
                {10, 20, 10, 20},
                {10, 20, 10, 20},
                {11, 12, 13, 14},
                {5, 5, 5, 5},
                {10, 10, 10, 10},
                {10, 10, 10, 10},
                {9, 9, 11, 11},
                {6, 6, 8, 8},
                {5, 5, 5, 5}
        };
        double[] result = {5, 10, 15, 15, 12.5, 5, 10, 10, 10, 7, 5};
        assertEquals("TaskCh12N063Test.testCountOfLearners", true,
                Arrays.equals(result, averageClass(testArray)));
    }

}
