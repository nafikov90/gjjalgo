package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh09N107 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the word");
        String word = scanner.nextLine();
        String result = replaceLetters(word);
        System.out.println(result);
    }

    public static String replaceLetters(String word) {
        if (word.contains("a") && word.contains("o")) {
            int indexOfA = word.indexOf("a");
            int indexOfO = word.lastIndexOf("o");
            char[] array = word.toCharArray();
            array[indexOfA] = 'o';
            array[indexOfO] = 'a';
            StringBuilder sb = new StringBuilder();
            for (char anArray : array) {
                sb.append(anArray);
            }
            return sb.toString();
        } else {
            return "these letters are missing";
        }
    }
}
