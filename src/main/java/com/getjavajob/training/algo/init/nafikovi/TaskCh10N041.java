package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh10N041 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number N");
        int number = scanner.nextInt();
        int result = factorial(number);
        System.out.println(result);
    }

    public static int factorial(int number) {
        int result;
        if (number == 1) {
            return 1;
        }
        result = factorial(number - 1) * number;
        return result;
    }
}
