package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh02N043 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number a");
        int a = scanner.nextInt();
        System.out.println("Enter the number b");
        int b = scanner.nextInt();
        int result = isDivisor(a, b);
        System.out.println(result);
    }

    public static int isDivisor(int a, int b) {
        return (a % b) * (b % a) + 1;
    }
}
