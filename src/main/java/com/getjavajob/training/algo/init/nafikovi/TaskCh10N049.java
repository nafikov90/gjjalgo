package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh10N049 {
    static final int ZERO_INDEX = 0;
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Enter the amount of elements in the array");
        int arrayLength = scanner.nextInt();
        int indexOFMaxEl = getIndexMaxEl(createArray(arrayLength), ZERO_INDEX);
        System.out.println("Index of the maximum element: " + indexOFMaxEl);
    }

    public static int getIndexMaxEl(int[] array, int index) {
        if (index == array.length - 1) {
            return index;
        }
        int indexOfMax = getIndexMaxEl(array, index + 1);
        return array[index] < array[indexOfMax] ? indexOfMax : index;
    }

    public static int[] createArray(int n) {
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.println("Enter " + i + " element of the array");
            array[i] = scanner.nextInt();
        }
        return array;
    }
}
