package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh11N158 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of elements in the array");
        int[] arr = new int[scanner.nextInt()];
        System.out.println("Enter the element values");
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.nextInt();
        }
        int[] result = deleteIdenticalElement(arr);
        printArr(result);
    }

    public static int[] deleteIdenticalElement(int[] arr) {
        int countOfDeletedElements = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] == arr[j]) {
                    deleteElement(arr, j);
                    countOfDeletedElements++;
                    if (i + countOfDeletedElements == arr.length - 1) {
                        return arr;
                    }
                }
            }
        }
        return arr;
    }

    public static int[] deleteElement(int[] arr, int num) {
        if (num == arr.length - 1) {
            arr[num] = 0;
            return arr;
        }
        for (int i = num; i < arr.length - 1; i++) {
            int tmp = arr[i];
            arr[i] = arr[i + 1];
            arr[i + 1] = tmp;
            if (i == arr.length - 2) {
                arr[i + 1] = 0;
            }
        }
        return arr;
    }

    static void printArr(int[] arr) {
        for (int x : arr) {
            System.out.print(x + " ");
        }
        System.out.println();
    }
}
