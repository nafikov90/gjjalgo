package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh11N245 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of elements in the array");
        int[] arr = new int[scanner.nextInt()];
        System.out.println("Enter the element values");
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.nextInt();
        }
        for (int x : arr) {
            System.out.print(x + " ");
        }
        System.out.println();
        int[] result = replaceElements1(arr);
        for (int x : result) {
            System.out.print(x + " ");
        }
    }

    public static int[] replaceElements1(int[] arr) {
        int[] newArr = new int[arr.length];
        int countOfNegative = 0;
        int countOfPositive = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < 0) {
                newArr[countOfNegative] = arr[i];
                countOfNegative++;
            } else {
                newArr[arr.length - 1 - countOfPositive] = arr[i];
                countOfPositive++;
            }
        }
        return newArr;
    }
}
