package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh04N106 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of month");
        int numberOfMonth = scanner.nextInt();
        String result = whatIsSeason(numberOfMonth);
        System.out.println(result);
    }

    public static String whatIsSeason(int numberOfMonth) {
        String season;
        switch (numberOfMonth) {
            case 1:
                season = "Winter";
                break;
            case 2:
                season = "Winter";
                break;
            case 3:
                season = "Spring";
                break;
            case 4:
                season = "Spring";
                break;
            case 5:
                season = "Spring";
                break;
            case 6:
                season = "Summer";
                break;
            case 7:
                season = "Summer";
                break;
            case 8:
                season = "Summer";
                break;
            case 9:
                season = "Autumn";
                break;
            case 10:
                season = "Autumn";
                break;
            case 11:
                season = "Autumn";
                break;
            case 12:
                season = "Winter";
                break;
            default:
                season = "Input Error";
                break;
        }
        return season;
    }
}
