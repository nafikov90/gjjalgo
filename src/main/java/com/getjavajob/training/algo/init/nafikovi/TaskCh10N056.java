package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh10N056 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number");
        int number = scanner.nextInt();
        boolean result = isPrime(number, number - 1);
        System.out.println(result);
    }

    public static boolean isPrime(int number, int divisor) {
        return divisor < 2 || number % divisor != 0 && isPrime(number, divisor - 1);
    }
}
