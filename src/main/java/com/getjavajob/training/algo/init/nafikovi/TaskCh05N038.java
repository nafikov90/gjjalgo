package com.getjavajob.training.algo.init.nafikovi;

public class TaskCh05N038 {
    private static final int NUMBER_OF_STAGES = 100;

    public static void main(String[] args) {
        String resultDistanceFromHome = distanceFromHome();
        System.out.println(resultDistanceFromHome);
        String resultTotalDistance = totalDistance();
        System.out.println(resultTotalDistance);
    }

    public static String distanceFromHome() {
        double toWork = 0;
        double toHome = 0;
        for (int i = 1; i <= NUMBER_OF_STAGES; i += 2) {
            toWork += 1.0 / i;
            toHome += 1.0 / (i + 1);
        }
        return String.format("%f", toWork - toHome);
    }

    public static String totalDistance() {
        double distance = 0;
        for (int i = 1; i <= NUMBER_OF_STAGES; i++) {
            distance += 1.0 / i;
        }
        return String.format("%f", distance);
    }
}
