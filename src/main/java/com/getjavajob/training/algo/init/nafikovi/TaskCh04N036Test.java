package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh04N036.colorSignal;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N036Test {
    public static void main(String[] args) {
        testColorRed();
        testColorGreen();
    }

    public static void testColorRed() {
        assertEquals("TaskCh04N036Test.testColorRed", true, colorSignal(3).equals("Red"));
    }

    public static void testColorGreen() {
        assertEquals("TaskCh04N036Test.testColorGreen", true, colorSignal(5).equals("Green"));
    }
}
