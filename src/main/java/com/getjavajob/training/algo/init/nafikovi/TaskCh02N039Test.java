package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh02N039.calculateAngle;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh02N039Test {
    public static void main(String[] args) {
        testAngle();
    }

    public static void testAngle() {
        assertEquals("TaskCh02N039Test.testAngle", 45, calculateAngle(1, 30, 0));
    }
}
