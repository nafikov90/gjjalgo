package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh03N029D.isTrue;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh03N029DTest {
    public static void main(String[] args) {
        testIsTrue1();
        testIsTrue2();
        testIsTrue3();
    }

    public static void testIsTrue1() {
        assertEquals("TaskCh03N029DTest.testIsTrue1", false, isTrue(1, 3, -5));
    }

    public static void testIsTrue2() {
        assertEquals("TaskCh03N029DTest.testIsTrue2", true, isTrue(-2, -3, -5));
    }

    public static void testIsTrue3() {
        assertEquals("TaskCh03N029DTest.testIsTrue3", false, isTrue(-1, 3, -5));
    }
}
