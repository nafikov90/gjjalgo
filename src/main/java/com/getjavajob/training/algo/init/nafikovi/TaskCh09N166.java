package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh09N166 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the sentence");
        String word = scanner.nextLine();
        String result = replaceWords(word);
        System.out.println(result);
    }

    public static String replaceWords(String word) {
        String[] arrOfWords = word.split(" ");
        String tmp = arrOfWords[0];
        arrOfWords[0] = arrOfWords[arrOfWords.length - 1];
        arrOfWords[arrOfWords.length - 1] = tmp;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < arrOfWords.length; i++) {
            sb.append(arrOfWords[i]).append(' ');
        }
        return sb.toString().trim();
    }
}
