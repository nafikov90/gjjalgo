package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh09N185 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the expression");
        String expression = scanner.nextLine();
        boolean result = isBracesTrue(expression);
        System.out.println(result);
        if (!result) {
            String excessBrace = isExcessBraces(expression);
            System.out.println(excessBrace);
        }
    }

    public static boolean isBracesTrue(String expression) {
        int countOpenBraces = 0;
        int countCloseBraces = 0;
        boolean bracesError = false;
        for (int i = 0; i < expression.length(); i++) {
            if (expression.charAt(i) == '(') {
                countOpenBraces++;
            } else if (expression.charAt(i) == ')') {
                countCloseBraces++;
            } else {
                continue;
            }
            if (countCloseBraces > countOpenBraces) {
                bracesError = true;
                break;
            }
        }
        return !bracesError && countCloseBraces == countOpenBraces;
    }

    public static String isExcessBraces(String s) {
        int countOpenBraces = 0;
        int countCloseBraces = 0;
        int numberCloseBraces = 0;
        boolean closeBracesExcess = false;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                countOpenBraces++;
            } else if (s.charAt(i) == ')') {
                countCloseBraces++;
            } else {
                continue;
            }
            if (countCloseBraces > countOpenBraces) {
                numberCloseBraces = i;
                closeBracesExcess = true;
                break;
            }
        }
        if (closeBracesExcess) {
            return "Excess closing brace " + numberCloseBraces;
        } else {
            return "Excess open brace " + (countOpenBraces - countCloseBraces);
        }
    }
}


