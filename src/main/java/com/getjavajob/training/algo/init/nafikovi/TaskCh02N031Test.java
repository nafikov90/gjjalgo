package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh02N031.findX;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh02N031Test {
    public static void main(String[] args) {
        testFind();
    }

    public static void testFind() {
        assertEquals("TaskCh02N031Test.testFind", 456, findX(465));
    }
}
