package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh10N050 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the n");
        int n = scanner.nextInt();
        System.out.println("Enter the m");
        int m = scanner.nextInt();
        int result = calcAck(n, m);
        System.out.println(result);
    }

    public static int calcAck(int n, int m) {
        if (n == 0) {
            return m + 1;
        } else if (m == 0) {
            return calcAck(n - 1, 1);
        } else {
            return calcAck(n - 1, calcAck(n, m - 1));
        }
    }
}
