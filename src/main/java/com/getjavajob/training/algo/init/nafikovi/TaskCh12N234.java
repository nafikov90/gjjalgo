package com.getjavajob.training.algo.init.nafikovi;

import java.util.Random;
import java.util.Scanner;

public class TaskCh12N234 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter n");
        int n = scanner.nextInt();
        System.out.println("Enter m");
        int m = scanner.nextInt();
        int[][] array = initArray(n, m);
        printArray(array);
        System.out.println();
        printArray(deleteColumn(array, 3));
        System.out.println();
        printArray(deleteLine(array, 2));
    }

    public static int[][] initArray(int n, int m) {
        int[][] array = new int[n][m];
        Random random = new Random();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = random.nextInt(9) + 1;
            }
        }
        return array;
    }

    public static void printArray(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static int[][] deleteLine(int[][] arr, int n) {
        int[] tmpArray;
        for (int i = n - 1; i < arr.length - 1; i++) {
            tmpArray = arr[i];
            arr[i] = arr[i + 1];
            arr[i + 1] = tmpArray;
        }
        for (int j = 0; j < arr[arr.length - 1].length; j++) {
            arr[arr.length - 1][j] = 0;
        }
        return arr;
    }

    public static int[][] deleteColumn(int[][] arr, int m) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = m - 1; j < arr[i].length - 1; j++) {
                arr[i][j] = arr[i][j + 1];
                arr[i][j + 1] = 0;
            }
        }
        return arr;
    }
}
