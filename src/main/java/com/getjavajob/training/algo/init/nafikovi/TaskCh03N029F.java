package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh03N029F {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number");
        int x = scanner.nextInt();
        System.out.println("Enter the number");
        int y = scanner.nextInt();
        System.out.println("Enter the number");
        int z = scanner.nextInt();
        boolean result = isTrue(x, y, z);
        System.out.println(result);
    }

    public static boolean isTrue(int x, int y, int z) {
        return x > 100 || y > 100 || z > 100;
    }
}
